package  com.prograd;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LastSundayOfEachMonthTest {
    @Test
    void shouldGiveLastSundayOfEachMonthWhenYearIsGiven(){
        LastSundayOfEachMonth lstSunday = new LastSundayOfEachMonth(2020);
        int[] expectedValue = {26,23,29,26,31,28,26,30,27,25,29,27};

        int[] actualValue = lstSunday.findLastSunday();

        assertThat(actualValue,is(equalTo(expectedValue)));
    }

    @Test
    void shouldGiveExceptionWhenInvalidYearIsGiven(){
        LastSundayOfEachMonth lstSunday = new LastSundayOfEachMonth(115);
        Assertions.assertThrows(IllegalArgumentException.class, () -> lstSunday.findLastSunday(), "Please enter a valid year with 4 digits");
    }

}

