package com.prograd;
public class LastSundayOfEachMonth {

    private final int year;

    public LastSundayOfEachMonth(int year){
        this.year = year;
    }


    public  int[] findLastSunday()
    {
        if(isValidYear(this.year)) {
            boolean isLeap = isLeapYear(year);

            int[] daysInAMonth = {31, isLeap ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            int[] dateOfLastSundayInEachMonth = new int[12];

            for (int month = 0; month < 12; month++) {
                int date = daysInAMonth[month];
                while (getWeekDay(year, month, date) != 0) {
                    date--;
                }
                dateOfLastSundayInEachMonth[month] = date;
            }
            return dateOfLastSundayInEachMonth;
        }
        else{
            throw new IllegalArgumentException("Please enter a valid year with 4 digits");
        }
    }


    private static boolean isLeapYear(int year)
    {
        if(year%4==0)
        {
            if(year%100!=0)
                return true;
            else if (year%400==0)
                return true;
        }
        return false;
    }

    private static int getWeekDay(int year, int month, int dateInMonth)
    {
        int day=year+dateInMonth+3*month-1;
        month++;

        if(month<3)
            year--;
        else
            day-=(int)(0.4*month+2.3);

        day+=(year/4)-(int)((year/100+1)*0.75);
        day%=7;
        return day;
    }

    private static boolean isValidYear(int year){
        int count = 0;
        while(year > 0){
            year /= 10;
            count++;
        }

        return count == 4;
    }

}
